from django.contrib.auth import authenticate, login, logout
from django.contrib.auth.decorators import login_required
from django.http import HttpRequest, HttpResponse, HttpResponseRedirect
from django.shortcuts import render, redirect
from django.urls import reverse

# from accounts.forms import CustomUserCreationForm


@login_required
def test(request: HttpRequest) -> HttpResponse:
    return HttpResponse("<h1>Test</h1>")


def login_view(request: HttpRequest) -> HttpResponse:
    if request.method == "GET":
        return render(request, "accounts/login.html")
    elif request.method == "POST":
        print(f"request data: {request.POST}") # - ніколи так не робіть!!!
        username = request.POST["username"]
        password = request.POST["password"]
        user = authenticate(username=username, password=password)
        if user:
            login(request, user)
            return HttpResponseRedirect(reverse("catalog:index"))
        else:
            error_context = {
                "error": "Invalid credentials."
            }
            return render(request, "accounts/login.html", context=error_context)


@login_required
def logout_view(request: HttpRequest) -> HttpResponse:
    logout(request)
    return render(request, "accounts/logged_out.html")
