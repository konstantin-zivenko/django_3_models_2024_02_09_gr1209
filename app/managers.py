import sqlite3

from app.models import Actor

DB_PATH = '/Users/kostyantynzivenko/Desktop/CBS/1209/django/django_3_models_2024_02_09_gr1209/app/actors.sqlite'


class ActorManager:
    def __init__(self):
        self.conn = sqlite3.connect(DB_PATH)
        self.table_name = 'actors'

    def all(self):
        sql = f'SELECT * FROM {self.table_name};'
        try:
            actors_cursor = self.conn.execute(sql)
            return [Actor(*row) for row in actors_cursor]
        except sqlite3.Error as error:
            print(f"ERROR: {error}")

    def create(self, first_name: str, last_name: str) -> None:
        sql = f'INSERT INTO {self.table_name}(first_name, last_name) VALUES (?, ?)'
        params = (first_name, last_name)
        try:
            self.conn.execute(sql, params)
        except sqlite3.Error as error:
            print(f"ERROR: {error}")
