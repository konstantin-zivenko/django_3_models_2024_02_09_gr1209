# django_3_models_2024_02_09_gr1209

---

[video 1](https://youtu.be/N8UMd-QoF3Y)

[video 2](https://youtu.be/SfsDVgFYg0k)

[video 3](https://youtu.be/f0TrZuP6gMM) views and templates

[video 4](https://youtu.be/56oijZXQ5v0) views and templates part 2

[video 5](https://youtu.be/Wk6Vqw9P4PM) pagination and sessions

[video 6](https://youtu.be/NOfb9zr8iwo) auth

[video 7](https://youtu.be/GpUrp3FYbV8) forms part 1

[video 8](https://youtu.be/CxQ_4OUNR6U) forms part 2

[video 9](https://youtu.be/ns7r8JSdi_s) simple search, REST intro

[video 10](https://youtu.be/w8JOaDzuViQ) serializers intro, @api_view

[video 11](https://youtu.be/GN5wDhPHdNw) APIView, GenericAPIView&Mixins? GenericView, ViewSets, routers

[video 12](https://youtu.be/4WJqpkEaVcM) project consultation: .env, flatpage, social auth

[video 13](https://youtu.be/256_dYKtW08) project consultation: deploy
