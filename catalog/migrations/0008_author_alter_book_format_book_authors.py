# Generated by Django 5.0.2 on 2024-02-13 18:49

import django.db.models.deletion
from django.db import migrations, models


class Migration(migrations.Migration):
    dependencies = [
        ("catalog", "0007_rename_format_literaryformat_name"),
    ]

    operations = [
        migrations.CreateModel(
            name="Author",
            fields=[
                (
                    "id",
                    models.BigAutoField(
                        auto_created=True,
                        primary_key=True,
                        serialize=False,
                        verbose_name="ID",
                    ),
                ),
                ("first_name", models.CharField(max_length=63)),
                ("last_name", models.CharField(max_length=63)),
            ],
        ),
        migrations.AlterField(
            model_name="book",
            name="format",
            field=models.ForeignKey(
                on_delete=django.db.models.deletion.CASCADE,
                related_name="books",
                to="catalog.literaryformat",
            ),
        ),
        migrations.AddField(
            model_name="book",
            name="authors",
            field=models.ManyToManyField(to="catalog.author"),
        ),
    ]
