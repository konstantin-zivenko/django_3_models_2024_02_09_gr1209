from django.urls import path
from .views import (
    index,
    LiteraryFormatListView,
    LiteraryFormatCreateView,
    LiteraryFormatUpdateView,
    LiteraryFormatDeleteView,
    BookListView,
    BookCreateView,
    BookDetailView,
    BookUpdateView,
    AuthorListView,
    AuthorDetailView
)

app_name = 'catalog'

urlpatterns = [
    path("", index, name="index"),
    path("books/", BookListView.as_view(), name="book_list"),
    path("books/create/", BookCreateView.as_view(), name="book_create"),
    path("books/<int:pk>/", BookDetailView.as_view(), name="book_detail"),
    path("books/<int:pk>/update/", BookUpdateView.as_view(), name="book_update"),
    path("authors/", AuthorListView.as_view(), name="author_list"),
    path("authors/<int:pk>/", AuthorDetailView.as_view(), name="author_detail"),
    path("literary-formats/", LiteraryFormatListView.as_view(), name="literary_format_list"),
    path("literary-formats/create/", LiteraryFormatCreateView.as_view(), name="literary_format_create"),
    path("literary-formats/<int:pk>/update/", LiteraryFormatUpdateView.as_view(), name="literary_format_update"),
    path("literary-formats/<int:pk>/delete/", LiteraryFormatDeleteView.as_view(), name="literary_format_delete"),
]
