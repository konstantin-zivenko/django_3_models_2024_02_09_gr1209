from django.urls import path, include
from .views import LiteraryFormatViewSet, AuthorViewSet, BookViewSet
from rest_framework import routers

router = routers.DefaultRouter()

router.register("literary_formats", LiteraryFormatViewSet)
router.register("authors", AuthorViewSet)
router.register("books", BookViewSet)


app_name = 'catalog_api'

urlpatterns = router.urls

# urlpatterns = [
#     path("", include(router.urls))
# ]
