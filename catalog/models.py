from datetime import datetime

from django.core.validators import MinLengthValidator
from django.db import models
from django.urls import reverse


class LiteraryFormat(models.Model):
    MIN_NAME_LENGTH=3
    name = models.CharField(
        max_length=63,
        unique=True,
        validators=[
            MinLengthValidator(MIN_NAME_LENGTH)
        ]
    )
    description = models.TextField(null=True, blank=True)

    def __str__(self):
        return self.name


class Author(models.Model):
    first_name = models.CharField(max_length=63)
    last_name = models.CharField(max_length=63)

    def __str__(self):
        return f"{self.first_name} {self.last_name} ({self.id})"

    class Meta:
        ordering = ["last_name"]

    def get_absolute_url(self):
        return reverse("catalog:author_detail", kwargs={"pk": self.pk})


class Book(models.Model):
    title = models.CharField(max_length=255)
    description = models.TextField(null=True, default="Very good book.")
    format = models.ForeignKey(LiteraryFormat, on_delete=models.CASCADE, related_name="books")
    added_at = models.DateTimeField(auto_now_add=True, null=True)
    authors = models.ManyToManyField(Author, related_name="books")

    def __str__(self):
        return self.title

    class Meta:
        ordering = ["title"]

    def get_absolute_url(self):
        return reverse("catalog:book_detail", kwargs={"pk": self.pk})
