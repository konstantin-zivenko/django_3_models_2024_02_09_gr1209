from django import forms
from django.core.exceptions import ValidationError
from django.core.validators import MinLengthValidator
from django.forms import CheckboxSelectMultiple

from catalog.models import LiteraryFormat, Book, Author


class LiteraryFormatForm(forms.ModelForm):
    # MIN_NAME_LENGTH = 3
    # name = forms.CharField(
    #     required=True,
    #     max_length=63,
    #     validators=[
    #         MinLengthValidator(MIN_NAME_LENGTH)
    #     ]
    # )
    class Meta:
        model = LiteraryFormat
        fields = ("name", "description")

    # def clean_name(self):
    #     MIN_NAME_LENGTH = 3
    #     name = self.cleaned_data["name"]
    #     if len(name) < MIN_NAME_LENGTH:
    #         raise ValidationError(f"the length of the literary format name must be more than {MIN_NAME_LENGTH} characters")
    #     return name
    #


class BookForm(forms.ModelForm):
    authors = forms.ModelMultipleChoiceField(
        queryset=Author.objects.all(),
        widget=CheckboxSelectMultiple,
        required=False,
    )
    format = forms.ModelChoiceField(
        queryset=LiteraryFormat.objects.all(),
        widget=CheckboxSelectMultiple,
    )
    class Meta:
        model = Book
        fields = "__all__"


class BookSearchForm(forms.Form):
    title = forms.CharField(
        max_length=255,
        required=False,
        label="",
        widget=forms.TextInput(
            attrs={"placeholder": "Search by title"}
        )
    )

