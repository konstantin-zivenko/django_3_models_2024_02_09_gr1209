import random

from catalog.models import Book, LiteraryFormat
from faker import Faker

fake = Faker()


def create_fake_book(num: int) -> list[Book]:
    literary_formats = LiteraryFormat.objects.all()
    fake_books = Book.objects.bulk_create(
        [
            Book(
                title=f"Test book {i} : {fake.sentence(nb_words=5, variable_nb_words=True)}",
                description= fake.paragraph(nb_sentences=4, variable_nb_sentences=True),
                format=random.choice(literary_formats)
            ) for i in range(num)
        ]
    )
    return fake_books
